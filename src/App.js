import './App.css';
import Button from './components/Button';
import Counter from './components/Counter';
import logoApp from './images/logo-2.png';
import { useState } from 'react';

function App() {

  const [clickNumber, setClickNumber] = useState(0);

  const incrementCounter = () => {
    setClickNumber(clickNumber + 1);
  };

  const resetCounter = () => {
    setClickNumber(0);
  };

  return (
    <div className="App">
      <div className='logo-container'>
        <img 
          className='logo'
          src={logoApp}
          alt='Logo del Sistema'/>
      </div>
      <div className='main-container'>
        <Counter clickNumber={clickNumber} />
        <Button 
          text='Click'
          isClickButton={true}
          manageClick={incrementCounter} />
        <Button 
          text='Reiniciar'
          isClickButton={false}
          manageClick={resetCounter} />
      </div>
    </div>
  );
}

export default App;
